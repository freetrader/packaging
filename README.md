Bitcoin Cash Node (BCHN) Packaging
==================================

This repository contains files used to package Bitcoin Cash Node (BCHN).

At the moment the only packaging files here are for Debian-based Linux
systems.

The files here are intended for use by BCHN maintainers to produce
installable packages for certain package distribution services (e.g. Ubuntu
Launchpad), but may also be useful if you compile BCHN from scratch and
want to produce packages yourself.
